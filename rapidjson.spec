%global debug_package %{nil}

Name:		rapidjson
Version:	1.1.0
Release:	30%{?dist}
Summary:	Fast JSON parser and generator for C++

# Most files are MIT, rapidjson/msinttypes/{stdint,inttypes}.h are BSD
License:	MIT AND BSD-3-Clause
URL:		http://rapidjson.org/
Source0:	https://github.com/Tencent/rapidjson/archive/v%{version}/%{name}-%{version}.tar.gz
# https://github.com/Tencent/rapidjson/pull/719
Patch:          0001-Removed-non-compiling-assignment-operator.-Fixed-718.patch
# https://github.com/Tencent/rapidjson/pull/719
Patch:          0002-Explicitly-disable-copy-assignment-operator.patch
# https://github.com/Tencent/rapidjson/pull/1137
Patch:          0003-Three-way-comparison-for-CLang-10-fix-1679.patch
# https://github.com/Tencent/rapidjson/pull/1679
Patch:          0004-Fix-recursive-operator-call-in-C-20-1846.patch
# https://github.com/Tencent/rapidjson/pull/1847
Patch:          0005-gate-definition-of-symmetric-equality-operators-on-i.patch
# https://github.com/Tencent/rapidjson/pull/2091
Patch:          0006-do-not-define-operator-in-C-20.patch
# Downstream-patch for gtest
Patch:          0007-do-not-include-gtest_src_dir.patch
# Make valgrind optional for riscv64
# https://github.com/Tencent/rapidjson/pull/2263
Patch:          0008-Make-valgrind-optional-for-riscv64.patch

BuildRequires:	cmake
BuildRequires:	make
BuildRequires:	gcc-c++
BuildRequires:	gtest-devel
%ifarch %{valgrind_arches}
BuildRequires:	valgrind
%endif
BuildRequires:	doxygen

%description
RapidJSON is a fast JSON parser and generator for C++.  It was		
inspired by RapidXml.							
									
  RapidJSON is small but complete.  It supports both SAX and DOM style	
  API. The SAX parser is only a half thousand lines of code.		
									
  RapidJSON is fast.  Its performance can be comparable to strlen().	
  It also optionally supports SSE2/SSE4.1 for acceleration.		
									
  RapidJSON is self-contained.  It does not depend on external		
  libraries such as BOOST.  It even does not depend on STL.		
									
  RapidJSON is memory friendly.  Each JSON value occupies exactly	
  16/20 bytes for most 32/64-bit machines (excluding text string).  By	
  default it uses a fast memory allocator, and the parser allocates	
  memory compactly during parsing.					
									
  RapidJSON is Unicode friendly.  It supports UTF-8, UTF-16, UTF-32	
  (LE & BE), and their detection, validation and transcoding		
  internally.  For example, you can read a UTF-8 file and let RapidJSON	
  transcode the JSON strings into UTF-16 in the DOM.  It also supports	
  surrogates and "\u0000" (null character).				
									
JSON(JavaScript Object Notation) is a light-weight data exchange	
format.  RapidJSON should be in fully compliance with RFC4627/ECMA-404.


%package devel
Summary:        %{summary}
Provides:	%{name}%{?_isa} = %{version}-%{release}
Provides:	%{name}-static = %{version}-%{release}

%description devel
%{description}


%package doc
Summary:	Documentation-files for %{name}
BuildArch:	noarch

%description doc
This package contains the documentation-files for %{name}.


%prep
%autosetup -p 1 -n %{name}-%{version}

# Remove bundled code
rm -rf thirdparty

# Convert DOS line endings to unix
for file in "license.txt" $(find example -type f -name *.c*)
do
  sed -e "s/\r$//g" < ${file} > ${file}.new && \
    touch -r ${file} ${file}.new && \
    mv -f ${file}.new ${file}
done

# Remove -march=native and -Werror from compile commands
find . -type f -name CMakeLists.txt -print0 | \
  xargs -0r sed -i -e "s/-march=native/ /g" -e "s/-Werror//g"


%build
%cmake \
    -DDOC_INSTALL_DIR:PATH=%{_pkgdocdir} \
    -DRAPIDJSON_BUILD_CXX11:BOOL=OFF \
    -DGTESTSRC_FOUND:BOOL=ON \
    -DGTEST_SOURCE_DIR:PATH=.
%cmake_build


%install
%cmake_install
install -pm 644 CHANGELOG.md readme*.md %{buildroot}%{_pkgdocdir}/
find %{buildroot} -type f -name 'CMake*.txt' -delete


%check
%ctest


%files devel
%license license.txt
%dir %{_pkgdocdir}
%{_pkgdocdir}/CHANGELOG.md
%{_pkgdocdir}/readme*.md
%{_libdir}/cmake/RapidJSON/
%{_libdir}/pkgconfig/*.pc
%{_includedir}/%{name}/


%files doc
%license license.txt
%{_pkgdocdir}/


%changelog
* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 1.1.0-30
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 1.1.0-29
- Bump release for June 2024 mass rebuild

* Sun Feb 25 2024 Richard W.M. Jones <rjones@redhat.com> - 1.1.0-28
- Bump and rebuild package (for riscv64)

* Fri Jan 26 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-27
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Mon Jan 22 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-26
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Thu Jan 18 2024 Tom Hughes <tom@compton.nu> - 1.1.0-25
- Add upstream patches for improved gcc 14 and C++20 support

* Fri Jan 05 2024 Honza Horak <hhorak@redhat.com> - 1.1.0-24
- SPDX migration
- Add BSD license that is used by stdint.h and inttypes.h

* Fri Jul 21 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-23
- Rebuilt for https://fedoraproject.org/wiki/Fedora_39_Mass_Rebuild

* Mon Jan 30 2023 Benjamin A. Beasley <code@musicinmybrain.net> - 1.1.0-22
- Do not force C++11: gtest 1.13.0 requires at least C++14

* Fri Jan 20 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-21
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Sat Jul 23 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-20
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Fri Jan 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-19
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Sun Jan 16 2022 Antonio Trande <sagitter@fedoraproject.org> - 1.1.0-18
- Build for EPEL9

* Fri Jul 23 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-17
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Wed Jan 27 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-16
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Tue Sep  1 2020 Tom Hughes <tom@compton.nu> - 1.1.0-15
- Add patch for C++20 support

* Wed Jul 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jul 14 2020 Tom Hughes <tom@compton.nu> - 1.1.0-13
- Install pkg-config and cmake files to arched location
- Build documentation as noarch

* Thu Jan 30 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Fri Jul 26 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Wed Jun 19 2019 Tom Hughes <tom@compton.nu> - 1.1.0-10
- Fix FTBS due to hardlink location change
- Tidy up spec file

* Sat Feb 02 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Wed Mar  7 2018 Tom Hughes <tom@compton.nu> - 1.1.0-7
- Require gcc-c++

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Aug  9 2017 Tom Hughes <tom@compton.nu> - 1.1.0-5
- Update valgrind exclusions

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri May 05 2017 Björn Esser <besser82@fedoraproject.org> - 1.1.0-2
- Doc-pkg must be build archful on RHEL <= 7

* Fri Feb 10 2017 Tom Hughes <tom@compton.nu> - 1.1.0-1
- Update to 1.1.0 upstream release
- Drop -march=native as ppc64 doesn't recognise it
- Exclude valgrind on aarch64 due to unhandled instruction in libgcc

* Sun Apr 03 2016 Björn Esser <fedora@besser82.io> - 1.0.2-1
- update to latest upstream-release (#1322941)

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.12-0.4.git20140801.67143c2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.12-0.3.git20140801.67143c2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.12-0.2.git20140801.67143c2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Wed Aug 06 2014 Björn Esser <bjoern.esser@gmail.com> - 0.12-0.1.git20140801.67143c2
- initial rpm release (#1127380)
